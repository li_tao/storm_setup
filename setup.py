from lxml import etree
from node import StormNode
import re
import sys


def main():
    setup_cluster(sys.argv[1])


def setup_cluster(config_file):
    nodes = build_nodes(config_file)
    # connect all
    for node in nodes:
        node.start_zk_server()
        node.start_storm()


def gen_confis(config_file):
    """ Generate storm and zookeeper config files. """
    nodes = build_nodes(config_file)
    gen_zkcfg(nodes, 'zoo.cfg')
    gen_storm_yaml(nodes, 'storm.yaml')


def gen_zkcfg(nodes, outfile):
    """ Currently only write hosts. """
    prefix = '''
tickTime=2000
initLimit=10
syncLimit=5
dataDir=/tmp/zookeeper
clientPort=2181
        '''
    outhandle = open(outfile, 'w')
    outhandle.write(prefix)
    outhandle.write('\n')
    server_conf = "server.%d=%s:2888:3888\n"
    for node in nodes:
        outhandle.write(server_conf % (node.node_id, node.host))
    outhandle.close()


def gen_storm_yaml(nodes, outfile):
    outhandle = open(outfile, 'w')
    nimbus_host = ""
    outhandle.write('storm.zookeeper.servers:\n')
    for node in nodes:
        outhandle.write('- "{}"\n'.format(node.host))
        if node.node_type == "nimbus":
            nimbus_host = node.host
    outhandle.write('storm.zookeeper.port: 2181\n')
    outhandle.write('nimbus.host: "{}"\n'.format(nimbus_host))
    outhandle.write('storm.local.dir: "/tmp/storm"\n')
    outhandle.write('storm.local.mode.zmq: true\n')
    outhandle.write('supervisor.slots.ports:\n- 6700\n- 6701\n- 6702\n')
    outhandle.write('- 6703')


def build_nodes(config_file):
    """ Read config file and setup storm cluster. """
    config = etree.parse(open(config_file, 'r')).getroot()
    # global attribute
    glob_conf = build_glob_conf_dict(config.xpath('glob'))

    node_configs = config.xpath('node')
    nodes = []
    for node_config in node_configs:
        assert len(node_config.xpath('id')) == 1
        assert len(node_config.xpath('host')) == 1
        range_re = re.compile("\[\d+\-\d+\]")
        _type = None
        type_ele = node_config.xpath('type')
        assert len(type_ele) == 1
        _type = type_ele[0].text
        _zk = get_attr(glob_conf, node_config, 'zk-path')
        _storm = get_attr(glob_conf, node_config, 'storm-path')
        _name = get_attr(glob_conf, node_config, 'username')

        node_id = node_config.xpath('id')[0].text
        host = node_config.xpath('host')[0].text
        if range_re.search(node_id) is not None and \
                range_re.search(host) is not None:
            ih_tuples = build_id_host_tuples(node_id, host)
            for ih in ih_tuples:
                _id = ih[0]
                _host = ih[1]
                nodes.append(StormNode(_id, _host, _type, _name, _zk, _storm))
        else:
            _id = int(node_id)
            nodes.append(StormNode(_id, host, _type, _name, _zk, _storm))
    return nodes


def get_attr(glob, local_tree, attr_name):
    ele = local_tree.xpath(attr_name)
    assert len(ele) <= 1
    attr = None
    if len(ele) == 1:
        attr = ele[0].text
    else:
        attr = glob[attr_name]
    assert attr is not None
    return attr


def build_glob_conf_dict(conf_xml_tree):
    assert len(conf_xml_tree) == 1
    glob_config = conf_xml_tree[0]
    glob_dict = {}
    for child in glob_config:
        glob_dict[child.tag] = child.text
    return glob_dict


def build_id_host_tuples(ids, hosts):
    """ ugly function to generate hosts and addrs. """
    regex = re.compile("\[(\d+)-(\d+)\]")
    res = regex.search(ids)
    from_id = int(res.groups()[0])
    to_id = int(res.groups()[1])
    res = regex.search(hosts)
    from_host = int(res.groups()[0])
    to_host = int(res.groups()[1])
    assert to_id - from_id == to_host - from_host
    id_host_tuples = [(from_id + i, regex.sub("%02d" % (from_host + i), hosts))
                      for i in range(to_id - from_id + 1)]
    return id_host_tuples


if __name__ == "__main__":
    main()
