import paramiko
import time

zk_path = "/homes/zhaozhou/LITao/storm_cslab/zookeeper/bin/zkServer.sh"
storm_path = "/homes/zhaozhou/LITao/storm_cslab/storm/bin/storm"


def main():
    # connect
    # Assign ID
    print("host1")
    host1 = paramiko.SSHClient()
    host1.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    host1.connect("csl2wk01.cse.ust.hk", username="zhaozhou")

    print("host2")
    host2 = paramiko.SSHClient()
    host2.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    host2.connect("csl2wk02.cse.ust.hk", username="zhaozhou")
    print("host3")
    host3 = paramiko.SSHClient()
    host3.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    host3.connect("csl2wk03.cse.ust.hk", username="zhaozhou")
    print("nodes connected")

    # assign id
    host1.exec_command("mkdir /tmp/zookeeper")
    host1.exec_command("echo 1 > /tmp/zookeeper/myid")
    host2.exec_command("mkdir /tmp/zookeeper")
    host2.exec_command("echo 2 > /tmp/zookeeper/myid")
    host3.exec_command("mkdir /tmp/zookeeper")
    host3.exec_command("echo 3 > /tmp/zookeeper/myid")
    print("id assigned")

    time.sleep(1)

    # start everything
    start_zk_cmd = zk_path + ' start'
    print(start_zk_cmd)
    host1.exec_command(start_zk_cmd)
    time.sleep(5)
    host2.exec_command(start_zk_cmd)
    time.sleep(5)
    host3.exec_command(start_zk_cmd)
    time.sleep(5)
    print("zookeeper started")

    # start storm
    cmd = storm_path + ' nimbus &'
    print(cmd)
    host1.exec_command(cmd)
    time.sleep(5)
    host1.exec_command(storm_path + ' supervisor &')
    time.sleep(5)
    host1.exec_command(storm_path + ' ui &')
    time.sleep(5)

    host2.exec_command(storm_path + ' supervisor &')
    time.sleep(5)
    host3.exec_command(storm_path + ' supervisor &')
    time.sleep(5)
    print("storm started")

if __name__ == "__main__":
    main()






