import paramiko
import os
import time


class StormNode:

    def __init__(self, _id, _host, _type, _name, _zk, _storm):
        self.host = _host
        self.node_id = _id
        self.node_type = _type
        self.username = _name
        self.zk_path = _zk
        self.storm_path = _storm
        self.connection = None
        self.gen_path()

    def __del__(self):
        if self.connection is not None:
            self.connection.close()

    def gen_path(self):
        self.zk_exe = os.path.join(self.zk_path, 'bin/zkServer.sh')
        self.storm_exe = os.path.join(self.storm_path, 'bin/storm')

    def assign_id(self):
        cmd = "econ {} > /tmp/zookeeper/myid".foramt(self.node_id)
        self.exec_command(cmd)

    def start_zk_server(self):
        self.exec_command('mkdir /tmp/zookeeper')
        set_id = "echo {} > /tmp/zookeeper/myid".format(self.node_id)
        self.exec_command(set_id)
        exec_count = 0
        loop_count = 0
        # make sure that everything is fine
        while not self.is_zk_server_ok():
            # loop no more than 20 times
            assert loop_count < 20
            loop_count += 1
            # resend the command for every 10 times
            if exec_count == 0 or loop_count % 5 == 0:
                cmd = self.zk_exe + " start"
                self.exec_command(cmd)
                exec_count += 1
            # wait for 3 seconds
            time.sleep(3)

    def start_storm(self):
        """ Currently just wait for certain time hand hope it is
        success."""
        assert self.is_zk_server_ok()
        if self.node_type == "nimbus":
            cmd = self.storm_exe + ' nimbus &'
            self.exec_nohang(cmd)
            time.sleep(10)
            cmd = self.storm_exe + ' ui &'
            self.exec_nohang(cmd)
            time.sleep(5)
        cmd = self.storm_exe + ' supervisor &'
        self.exec_nohang(cmd)
        # wait for startup
        time.sleep(10)

    def stop_zk_server(self):
        if self.is_zk_server_ok():
            cmd = self.zk_exe + " stop"
            self.exec_command(cmd)

    def is_zk_server_ok(self):
        cmd = "echo ruok|nc localhost 2181"
        out = self.exec_command(cmd)
        out_str = str(out, encoding='utf8')
        if 'imok' in out_str:
            return True
        else:
            return False

    def connect(self):
        if self.connection is None:
            self.connection = paramiko.SSHClient()
            self.connection.set_missing_host_key_policy(
                paramiko.AutoAddPolicy())
            self.connection.connect(self.host, username=self.username)

    def exec_command(self, cmd):
        self.connect()
        print("exec - {}:{}".format(self.host, cmd))
        _, stdout, _ = self.connection.exec_command(cmd)
        return stdout.read()

    def exec_nohang(self, cmd):
        self.connect()
        print("exec - {}:{}".format(self.host, cmd))
        self.connection.exec_command(cmd)

    def __str__(self):
        return "id:{}, host:{}, type:{}, name:{}, zk:{}, storm:{}".format(
            self.node_id, self.host, self.node_type,  self.username,
            self.zk_path, self.storm_path)
